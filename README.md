## How to use

This is a mandatory guide for the developers that want to create a module/app for the residence but don't want to use the react/redux version. It is advised that this guide is more hard than the react/redux and has less utilities than the other.

It is recommended to check out the [react/redux](https://gitlab.com/resiajf/web/react-scripts-resi) version to know some of the utilities and structure available in this repository.

> Angular 6 required

> **Note**: use `scss` for styles when creating the Angular module

> **Note**: This tutorial only works on Linux, macOS and Unix-like OS. Avoid Windows for this...

1. Run this commands after creating the module:

```bash
git submodule init
git submodule add -f git@gitlab.com:resiajf/web/app-common-content.git common
git submodule add -f git@gitlab.com:resiajf/web/app-common-content-ng.git src/common
npm i @types/node codelyzer bootstrap hamburgers jquery popper.js angular-i18next i18next i18next-browser-languagedetector i18next-xhr-backend
rm src/app/* #yes, you don't need anything from the template ;)
```

2. Create the file `src/angular.module.config.ts` with this **basic** content:

```typescript
import { ModuleWithProviders, Provider, Type } from '@angular/core';
import { Route } from './common/utils.angular';
import { TestComponent } from './app/test.component';

export const module = ''; // TODO set module name
export const anonymousModule = false;

export const routes: Route[] = [];

export const declarations: Array<Type<any> | any[]> = [TestComponent];

export const providers: Provider[] = [];

export const imports: Array<Type<any> | ModuleWithProviders<{}> | any[]> = [];
```

3. Modify `src/main.ts`:
   1. Look for the line where `AppModule` is imported (like line 3)
   2. change the path to `'./common/app.module'`

4. Append the next line to `src/styles.scss`:

```scss
@import 'common/app/app.component';
```

5. Create the file `proxy.conf.json` with the contents:

```json
{
  "/app-api": {
    "target": "http://localhost:3001",
    "secure": false
  },
  "/locales": {
    "target": "http://localhost:3001",
    "secure": false
  },
  "/icons": {
    "target": "http://localhost:3001",
    "secure": false
  }
}
```

6. Modify `angular.json`:
   1. Look for the line `"browserTarget": "app-common:build"`, inside `serve` > `options`
   2. Add a new item in the object with the key `proxyConfig` and value `"proxy.conf.json"`
   3. It should look like:
```json
...
          "builder": "@angular-devkit/build-angular:dev-server",
          "options": {
            "browserTarget": "app-common:build",
            "proxyConfig": "proxy.conf.json"
          },
...
```

7. Modify `tsconfig.json`:
   1. Add inside the object `compilerOptions`, after `lib` the following: `"types": ["node"]`
   2. Add after the object `compilerOptions` the following `"include": ["common/lib/api/**/*.ts", "common/lib/navigator-storage/**/*.ts", "common/lib/api/http-client.ts", "common/lib/meta.ts", "common/lib/session.ts", "src"]`

8. To avoid problems with the common code and `tslint`, modify `tslint.json`:
   1. Look for `comment-format` and set its value to `[false]`
   2. Look for `no-non-null-assertion` and set its value to `false`
   3. Look for `whitespace` and set its value to `[false]`
   
## Mock server

If you don't intend to use the Antonio's Python backend, it is recommended to build a mockup server that:

 1. Implements the routes `/app-api/usuario`
 2. Publishes the repository of [locales](https://gitlab.com/resiajf/web/locales) in the path `/locales`
 3. Publishes the repository of [icons](https://gitlab.com/resiajf/web/icons) in the path `/icons`
 4. Listens to port `3001` (recommended, but you can change it in the proxy configuration)

## Appendix: patches

> You **must** run the step 1 from the first section before doing anything here.

Here is a list of patches to do almost the same as before, but automatically. To apply one patch, run `git apply`, paste one, then press ENTER and then `Ctrl + D`.

### angular.json

```
diff --git a/angular.json b/angular.json
index d25139c..eb7b437 100644
--- a/angular.json
+++ b/angular.json
@@ -61,7 +61,8 @@
         "serve": {
           "builder": "@angular-devkit/build-angular:dev-server",
           "options": {
-            "browserTarget": "app-common:build"
+            "browserTarget": "app-common:build",
+            "proxyConfig": "proxy.conf.json"
           },
           "configurations": {
             "production": {
@@ -136,4 +137,4 @@
     }
   },
   "defaultProject": "app-common"
-}
\ No newline at end of file
+}
```

### proxy.conf.json

```
diff --git a/proxy.conf.json b/proxy.conf.json
index e69de29..ee7f715 100644
--- a/proxy.conf.json
+++ b/proxy.conf.json
@@ -0,0 +1,10 @@
+{
+  "/app-api": {
+    "target": "http://localhost:3001",
+    "secure": false
+  },
+  "/locales": {
+    "target": "http://localhost:3001",
+    "secure": false
+  }
+}
```

### src/angular.module.config.ts

```
diff --git a/src/angular.module.config.ts b/src/angular.module.config.ts
new file mode 100644
index 0000000..8632324
--- /dev/null
+++ b/src/angular.module.config.ts
@@ -0,0 +1,13 @@
+import { ModuleWithProviders, Provider, Type } from '@angular/core';
+import { Route } from './common/utils.angular';
+import { TestComponent } from './app/test.component';
+
+export const module = ''; // TODO
+export const anonymousModule = false;
+
+export const routes: Route[] = [];
+
+export const declarations: Array<Type<any> | any[]> = [TestComponent];
+
+export const providers: Provider[] = [];
+
+export const imports: Array<Type<any> | ModuleWithProviders<{}> | any[]> = [];
```

### src/main.ts

```
diff --git a/src/main.ts b/src/main.ts
index c7b673c..32a1abb 100644
--- a/src/main.ts
+++ b/src/main.ts
@@ -1,7 +1,7 @@
 import { enableProdMode } from '@angular/core';
 import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
 
-import { AppModule } from './app/app.module';
+import { AppModule } from './common/app.module';
 import { environment } from './environments/environment';
 
 if (environment.production) {
@@ -10,3 +10,4 @@ if (environment.production) {
 
 platformBrowserDynamic().bootstrapModule(AppModule)
   .catch(err => console.error(err));
+
```

### src/styles.scss

```
diff --git a/src/styles.scss b/src/styles.scss
index 90d4ee0..ba6fb52 100644
--- a/src/styles.scss
+++ b/src/styles.scss
@@ -1 +1,2 @@
 /* You can add global styles to this file, and also import other style files */
+@import 'common/app/app.component';
```

### tsconfig.json

```
diff --git a/tsconfig.json b/tsconfig.json
index 46aeded..d83a2b7 100644
--- a/tsconfig.json
+++ b/tsconfig.json
@@ -16,6 +16,8 @@
     "lib": [
       "es2018",
       "dom"
-    ]
-  }
+    ],
+    "types": ["node"]
+  },
+  "include": ["common/lib/api/**/*.ts", "common/lib/navigator-storage/**/*.ts", "common/lib/api/http-client.ts", "common/lib/meta.ts", "common/lib/session.ts", "src"]
 }

```

### tslint.json

```
diff --git a/tslint.json b/tslint.json
index 6ddb6b2..cb97e82 100644
--- a/tslint.json
+++ b/tslint.json
@@ -7,8 +7,7 @@
     "callable-types": true,
     "class-name": true,
     "comment-format": [
-      true,
-      "check-space"
+      false
     ],
     "curly": true,
     "deprecation": {
@@ -64,7 +63,7 @@
       "ignore-params"
     ],
     "no-misused-new": true,
-    "no-non-null-assertion": true,
+    "no-non-null-assertion": false,
     "no-redundant-jsdoc": true,
     "no-shadowed-variable": true,
     "no-string-literal": false,
@@ -110,7 +109,7 @@
     "unified-signatures": true,
     "variable-name": false,
     "whitespace": [
-      true,
+      false,
       "check-branch",
       "check-decl",
       "check-operator",
```
