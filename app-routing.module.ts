import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from '../angular.module.config';
import { checkRoutes } from './utils.angular';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

checkRoutes(routes);

@NgModule({
  imports: [RouterModule.forRoot([ ...routes, { path: '**', component: PageNotFoundComponent } ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
