import { Route as NgRoute } from '@angular/router';
import { environment } from '../environments/environment';

export interface Route extends NgRoute {
  /**
   * Associates the route with a `page` permission key from the users permissions. More than one route could have the
   * same value for this.
   */
  pagePermissionKey: string;

  /**
   * A route identifier. Useful for translations. This must be unique.
   */
  routeKey: string;

  /**
   * Hide this route from the UI
   */
  hide?: boolean;
}

export function checkRoutes(routes: Route[]) {
    if(environment.production) {
        routes.forEach(r => {
            //Route permission keys must be non empty
            if(r.pagePermissionKey === '') {
              throw new Error(`Page permission key for this route is empty: ${JSON.stringify(r)}`);
            }

            //Route keys must be non empty
            if(r.routeKey === '') {
              throw new Error(`Route key for this route is empty: ${JSON.stringify(r)}`);
            }

            //Route keys must be unique
            const sameKeys = routes.filter(({ routeKey }) => routeKey === r.routeKey);
            if(sameKeys.length > 1) {
              throw new Error(`The route key ${r.routeKey} is not unique: ${JSON.stringify(sameKeys)}`);
            }
        });
    }
}

/**
 * Debounce the execution of a function. _Debouncing_ reduces the calls of a function when a event is fired with a huge
 * speed (like a window resize or a range input changing). This way, the function is called less, reducing the CPU usage
 * of the web page when some event is fired.
 * @param func Function to debounce.
 * @param wait The time that the function will wait until it will be called.
 * @param immediate Calls the function at the start of the wait, instead of the end.
 * @see https://stackoverflow.com/questions/24004791/can-someone-explain-the-debounce-function-in-javascript#24004942
 * @see https://davidwalsh.name/javascript-debounce-function
 * @example
 * const onClickDebounced = debounce((e: React.MouseEvent<HTMLButtonElement>) => {
 *     e.preventDefault();
 *     //Do something
 * }, 1000);
 * <Button type="primary" onClick={ onClickDebounced }>Does something</Button>
 * //Now, if you press the button, it will call the event when 1s passes. If you click more times, it will reset the
 * //timeout. This way, it will be called, but less times, improving the performance.
 */
export function debounce(func: (...args: any[]) => any, wait: number = 100, immediate: boolean = false) {
    let timeout: number | undefined;
    return function(this: any) {
        const context = this;
        const args = arguments;
        const callNow = immediate && !timeout;
        clearTimeout(timeout);

        timeout = setTimeout(() => {
            timeout = undefined;
            if(!immediate) {
                func.apply(context, args);
            }
        }, wait) as any;

        if(callNow) {
            func.apply(context, args);
        }
    };
}
