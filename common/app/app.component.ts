import { Component, Inject } from '@angular/core';

import { module, routes } from '../../angular.module.config';
import { UserService } from '../user.service';
import { Route } from '../utils.angular';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent {
    title = module;

    public userLoaded = false;
    public offCanvas = false;
    public mRoutes: Route[];

    constructor(private userService: UserService) {
        userService.userReceived.subscribe(user => {
            const canSeeThisModule = user.modules.indexOf(module) !== -1;
            this.mRoutes = canSeeThisModule ? routes.filter(route => user.pages.indexOf(route.pagePermissionKey) !== -1) : [];
            this.userLoaded = true;
        });
    }

    private toggleOffCanvas() {
        this.offCanvas = !this.offCanvas;
    }
}
