import { Component, Inject, Input } from '@angular/core';
import { module } from '../../angular.module.config';
import { Route } from '../utils.angular';
import { I18NEXT_SERVICE, ITranslationService } from 'angular-i18next';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent {
    @Input()
    public toggleOffCanvas: () => void;

    @Input()
    public offCanvas: boolean;

    private _modules: string[];
    private _routes: Route[];
    private modules2: string[];
    private routesFiltered: Route[];
    private moduleName = module;

    constructor(@Inject(I18NEXT_SERVICE) private i18NextService: ITranslationService) {}

    @Input()
    public get modules() {
        return this._modules;
    }

    public set modules(modules: string[]) {
        const t = (str: string) => this.i18NextService.t(str, { defaultValue: str });
        this._modules = modules;
        this.modules2 = this.modules.sort((a, b) => t(`modules.${a}.title`).localeCompare(t(`modules.${b}.title`)));
    }

    @Input()
    public get routes() {
        return this._routes;
    }

    public set routes(routes: Route[]) {
        this._routes = routes;
        this.routesFiltered = this.routes.filter(route => !route.hide);
    }

    private isLinkActive(route: Route) {
        return false;
    }

    private navLinkPressed() {
        this.toggleOffCanvas();
        return true;
    }
}
