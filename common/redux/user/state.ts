export interface User {
    niu: number;
    displayName: string;
    apartment: number | undefined;
    rol: number;
    modules: string[];
    pages: string[];
    pagesPerModule: { [index: string]: string[] };
    permissionsPerPage: { [index: string]: string[] };
    numNotifications?: number;
}