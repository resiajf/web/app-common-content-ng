import { Component, Input } from '@angular/core';
import { UserService } from '../user.service';
import { Session } from '../lib/session';
import { module } from '../../angular.module.config';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    @Input()
    public toggleOffCanvas: () => void;

    @Input()
    public offCanvas: boolean;

    module = module;

    constructor(private userService: UserService) { }

    private closeSession() {
        Session.deleteSession();
    }
}
