import { Device } from 'src/common/lib/api';
import {
    InstantSpeedParams,
    NetworkSpeedDataRow,
    NetworkSpeedDataRowUnfilled,
    NetworkSpeedRow,
    NetworkSpeedRowUnfilled,
} from 'src/common/lib/api/network-speed/interfaces';
import { get } from '../../http-client';

export class NetworkSpeed {

    public static async instant(device: Device, params: InstantSpeedParams, fill: true): Promise<NetworkSpeedRow[]>;
    public static async instant(device: Device, params: InstantSpeedParams, fill: false): Promise<NetworkSpeedRowUnfilled[]>;
    public static async instant(device: Device, params: InstantSpeedParams, fill: boolean): Promise<NetworkSpeedRowUnfilled[] | NetworkSpeedRow[]>;
    public static async instant(device: Device, params: InstantSpeedParams, fill: boolean = false) {
        const p = { ...params, fill };
        if(fill) {
            return (await get<{ results: NetworkSpeedRow[] }>(`consumo-internet/${device.id_dispositivo}`, p)).results;
        } else {
            return (await get<{ results: NetworkSpeedRowUnfilled[] }>(`consumo-internet/${device.id_dispositivo}`, p)).results;
        }
    }

    public static async week(device: Device, fill: true): Promise<NetworkSpeedDataRow[]>;
    public static async week(device: Device, fill: false): Promise<NetworkSpeedDataRowUnfilled[]>;
    public static async week(device: Device, fill: boolean): Promise<NetworkSpeedDataRowUnfilled[] | NetworkSpeedDataRow[]>;
    public static async week(device: Device, fill: boolean = false) {
        if(fill) {
            return (await get<{ results: NetworkSpeedDataRow[] }>(`consumo-internet/${device.id_dispositivo}/week`, { fill })).results;
        } else {
            return (await get<{ results: NetworkSpeedDataRowUnfilled[] }>(`consumo-internet/${device.id_dispositivo}/week`, { fill })).results;
        }
    }

    public static async month(device: Device, fill: true): Promise<NetworkSpeedDataRow[]>;
    public static async month(device: Device, fill: false): Promise<NetworkSpeedDataRowUnfilled[]>;
    public static async month(device: Device, fill: boolean): Promise<NetworkSpeedDataRowUnfilled[] | NetworkSpeedDataRow[]>;
    public static async month(device: Device, fill: boolean = false) {
        if(fill) {
            return (await get<{ results: NetworkSpeedDataRow[] }>(`consumo-internet/${device.id_dispositivo}/month`, { fill })).results;
        } else {
            return (await get<{ results: NetworkSpeedDataRowUnfilled[] }>(`consumo-internet/${device.id_dispositivo}/month`, { fill })).results;
        }
    }

}
