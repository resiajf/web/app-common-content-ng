export interface NetworkSpeedRow {
    time: Date;
    download: number;
    upload: number;
}

export interface NetworkSpeedRowUnfilled {
    time: Date;
    download: number | null;
    upload: number | null;
}

export interface NetworkSpeedDataRow {
    time: Date;
    avg_download: number;
    total_download: number;
    avg_upload: number;
    total_upload: number;
}

export interface NetworkSpeedDataRowUnfilled {
    time: Date;
    avg_download: number | null;
    total_download: number | null;
    avg_upload: number | null;
    total_upload: number | null;
}

export interface InstantSpeedParams {
    precision?: 0 | 1 | 2 | 3 | 4;
    from_param?: number;
    to_param?: number;
    last?: boolean;
}