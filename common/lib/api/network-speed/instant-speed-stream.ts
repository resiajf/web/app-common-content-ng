import { Device, NetworkSpeed } from 'src/common/lib/api';

import { EventEmitter } from 'events';

/**
 * Creates a read stream of real-time network speed for a specific device. When started, it will send you a bunch of
 * points to have an initial data, and then, it will send one by one the new points. When stopped, or when a property
 * is changed, the stream will be reset and will start again.
 *
 * When an error occurs, the stream is stopped to avoid repeating the same error. But, anytime, you could restart the
 * stream.
 *
 * The data is ordered from newest to oldest, with type `NetworkSpeedRow[]`.
 *
 * Events: `'data'`, `'reset'`, `'error'`. Use `on(event: string, callback: (...args: any[]) => any)` to listen to
 * events.
 */
export class InstantSpeedStream extends EventEmitter {

    private static checkFrom(from: number) {
        if(from < 30 || from > 86400) {
            throw new TypeError(`Invalid value for 'from': ${from}. Must be inside [3600, 86400].`);
        }
    }

    private static checkTo(to: number) {
        if(to < 0 || to > 82800) {
            throw new TypeError(`Invalid value for 'to': ${to}. Must be inside [0, 82800].`);
        }
    }

    private static checkRange(from: number, to: number) {
        if(from - to < 30) {
            throw new TypeError(`Invalid range of dates: [${from}, ${to}]. Range must be minimum of 30 seconds difference.`);
        }
    }

    private precision: 0 | 1 | 2 | 3 | 4;
    private timer: any | null;
    private goodTime: number = +new Date();

    constructor(
        private dev: Device,
        private from: number,
        private to: number,
        private fill: boolean
    ) {
        super();
        InstantSpeedStream.checkFrom(this.from);
        InstantSpeedStream.checkTo(this.to);
        InstantSpeedStream.checkRange(this.from, this.to);
        this.calculatePrecision();
        this.timer = null;

        this.onTimerTick = this.onTimerTick.bind(this);
    }

    public start(): this {
        this.setTimer();
        return this;
    }

    public stop(): this {
        if(this.timer !== null) {
            this.emit('reset');
            clearTimeout(this.timer);
            this.timer = null;
        }

        return this;
    }

    public set fromSeconds(val: number) {
        InstantSpeedStream.checkFrom(val);
        InstantSpeedStream.checkRange(val, this.to);
        this.from = val;
        this.calculatePrecision();
        this.setTimer();
    }

    public set toSeconds(val: number) {
        InstantSpeedStream.checkTo(val);
        InstantSpeedStream.checkRange(this.from, val);
        this.to = val;
        this.calculatePrecision();
        this.setTimer();
    }

    public set fillWith0(val: boolean) {
        this.fill = val;
        this.setTimer();
    }

    public set device(device: Device) {
        this.dev = device;
        this.setTimer();
    }

    private setTimer() {
        this.stop();
        this.goodTime = +new Date() + this.precisionAsTime;
        NetworkSpeed.instant(this.dev, {
            from_param: this.from,
            precision: this.precision,
            to_param: this.to,
        }, this.fill)
            .then(data => this.emit('data', data.slice(1)))
            .catch(error => {
                this.emit('error', error);
                clearTimeout(this.timer!);
            });
        this.timer = setTimeout(this.onTimerTick, this.precisionAsTime);
    }

    private onTimerTick() {
        const diff = +new Date() - +this.goodTime;
        this.goodTime = +new Date() + this.precisionAsTime - diff;
        this.timer = setTimeout(this.onTimerTick, this.precisionAsTime - diff);
        NetworkSpeed.instant(this.dev, {
            from_param: this.from,
            last: true,
            precision: this.precision,
            to_param: this.to,
        }, this.fill)
            .then(data => this.emit('data', data))
            .catch(error => {
                this.emit('error', error);
                clearTimeout(this.timer!);
            });
    }

    private calculatePrecision() {
        const diff = this.from - this.to;
        if(diff <= 900) {
            this.precision = 0;
        } else if(diff <= 2400) {
            this.precision = 1;
        } else  if(diff <= 3600) {
            this.precision = 2;
        } else  if(diff <= 2*3600) {
            this.precision = 3;
        } else {
            this.precision = 4;
        }
    }

    private get precisionAsTime() {
        switch(this.precision) {
            case 0: return  1000;
            case 1: return  5000;
            case 2: return 15000;
            case 3: return 30000;
            case 4: return 60000;
        }
    }

}
