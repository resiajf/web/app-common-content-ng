import { SelfUser, User } from 'src/common/lib/api/users/interfaces';
import { get } from 'src/common/lib/http-client';

export class Users {

    public static async usuario() {
        return await get<SelfUser>('usuario');
    }

    public static async getUser(id: string) {
        return await get<User>('usuarios/' + id);
    }

}