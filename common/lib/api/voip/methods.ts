import { get } from 'src/common/lib/http-client';

import { CallHistory } from 'src/common/lib/api/voip/interfaces';

export interface GetListParameters {
    from_date?: Date;
    to_date?: Date;
    get_all_items?: boolean;
    extension_f1?: number;
    extension_f2?: string;
}

export class VoIP {

    public static async getList(page: number, options: GetListParameters = {}) {
        return await get<CallHistory>('llamadas', {
            page,
            ...options,
        });
    }

}