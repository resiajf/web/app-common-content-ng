export interface CallRecord {
    datetime: Date;
    to_extension: string;
    from_extension: string;
    call_length: number;
}

export interface CallHistory {
    start: number;
    limit: number;
    count: number;
    pages: number;
    results: CallRecord[];
}