import { del, get, patch, ws } from 'src/common/lib/http-client';

import {
    Notification,
    NotificationConfigBase,
    NotificationConfigTelegram,
    NotificationConfigTelegramTest,
    NotificationList
} from 'src/common/lib/api';
import { NewNotificationsChannel } from 'src/common/lib/api/notifications/new-notifications-channel';

export class Notifications {

    public static async getList(page: number, unreadOnly: boolean = false) {
        return await get<NotificationList>('notificaciones', { page, unread_only: unreadOnly });
    }

    public static async getOne(id: Notification | number) {
        if(typeof id !== 'number') {
            id = id.id_notificacion;
        }
        return await get<Notification>(`notificaciones/${id}`);
    }

    public static async markAsRead(id: Notification | number) {
        if(typeof id !== 'number') {
            id = id.id_notificacion;
        }
        return await patch<Notification, {}>(`notificaciones/${id}`, { read: true });
    }

    public static async discard(id: Notification | number) {
        if(typeof id !== 'number') {
            id = id.id_notificacion;
        }
        return await del<Notification>(`notificaciones/${id}`);
    }

    public static async getConfigEmail() {
        return await get<NotificationConfigBase>(`notificaciones/configuracion/correo`);
    }

    public static async getConfigTelegram() {
        return await get<NotificationConfigTelegram>(`notificaciones/configuracion/telegram`);
    }

    public static async modifyConfigEmail(enabled: boolean) {
        return await patch<NotificationConfigBase, NotificationConfigBase>(`notificaciones/configuracion/correo`, { enabled });
    }

    public static async modifyConfigTelegram(values: Partial<NotificationConfigTelegram>) {
        return await patch<NotificationConfigTelegram, NotificationConfigTelegram>(`notificaciones/configuracion/telegram`, values);
    }

    public static async testEnabledTelegram() {
        return await get<NotificationConfigTelegramTest>(`notificaciones/configuracion/telegram/test`);
    }

    public static newNotificationsChannel() {
        return new NewNotificationsChannel(ws('notifications/ws'));
    }

}