export class UnknownServerError {

    constructor(
        public readonly res: Response,
        public readonly body: string | any,
    ) {}

    public get status(): number {
        return this.res.status;
    }

    public get contentType(): string {
        return this.res.headers.has('Content-Type') ? this.res.headers.get('Content-Type')! : 'text/plain';
    }

}