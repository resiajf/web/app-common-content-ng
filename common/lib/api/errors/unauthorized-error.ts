import { ErrorBase, ErrorBaseType } from 'src/common/lib/api/errors/error.interface';

export class UnauthorizedError implements ErrorBase<'Unauthorized'> {
    public readonly title: string;
    public readonly type: number;
    public readonly detail: string;
    public readonly kind = 'Unauthorized';

    public constructor(obj: ErrorBaseType) {
        this.type = Number(obj.type) || -1;
        this.title = obj.title || '';
        this.detail = obj.detail || '';
    }

    public get translatedMessageKey(): string {
        switch(this.type) {
            case 150: return 'errors.unauthorized.150';
            case 151: return 'errors.unauthorized.151';
            case 152: return 'errors.unauthorized.152';
            case 153: return 'errors.unauthorized.153';
            case 550: return 'errors.unauthorized.550';
            default:
                return 'errors.unauthorized.default';
        }
    }
}