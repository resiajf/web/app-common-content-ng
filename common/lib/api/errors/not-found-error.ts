import { ErrorBase, ErrorBaseType } from 'src/common/lib/api/errors/error.interface';

export class NotFoundError implements ErrorBase<'NotFound'> {
    public readonly title: string;
    public readonly type: number;
    public readonly detail: string;
    public readonly kind: 'NotFound';

    public constructor(obj: ErrorBaseType) {
        this.type = Number(obj.type) || -1;
        this.title = obj.title || '';
        this.detail = obj.detail || '';
    }

    public get translatedMessageKey(): string {
        switch(this.type) {
            case 450: return 'errors.not-found.450';
            case 451: return 'errors.not-found.451';
            case 453: return 'errors.not-found.453';
            case 454: return 'errors.not-found.454';
            case 455: return 'errors.not-found.455';
            case 457: return 'errors.not-found.457';
            case 461: return 'errors.not-found.461';
            default:
                return 'errors.not-found.default';
        }
    }

}