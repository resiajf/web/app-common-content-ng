import { ErrorBase, ErrorBaseType } from 'src/common/lib/api/errors/error.interface';

export class ForbiddenError implements ErrorBase<'Forbidden'> {
    public readonly title: string;
    public readonly type: number;
    public readonly detail: string;
    public readonly kind = 'Forbidden';

    public constructor(obj: ErrorBaseType) {
        this.type = Number(obj.type) || -1;
        this.title = obj.title || '';
        this.detail = obj.detail || '';
    }

    public get translatedMessageKey(): string {
        switch(this.type) {
            case 452: return 'errors.forbidden.452';
            case 456: return 'errors.forbidden.456';
            case 458: return 'errors.forbidden.458';
            case 462: return 'errors.forbidden.462';
            default:
                return 'errors.forbidden.default';
        }
    }
}