import { ErrorBase, ErrorBaseType } from 'src/common/lib/api/errors/error.interface';

export class ServerUnavailableError implements ErrorBase<'ServerUnavailable'> {
    public readonly title: string;
    public readonly type: number;
    public readonly detail: string;
    public readonly status: number;
    public readonly kind = 'ServerUnavailable';

    public constructor(obj: ErrorBaseType) {
        this.type = Number(obj.type) || -1;
        this.title = obj.title || '';
        this.detail = obj.detail || '';
        this.status = obj.status;
    }

    public get translatedMessageKey(): string {
        switch(this.type) {
            default:
                return 'errors.unavailable.default';
        }
    }
}