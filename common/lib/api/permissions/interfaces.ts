export interface Permission {
    id_permiso: number;
    module_name: string;
    page_name: string;
    permission: string;
    permission_desc: string;
}

export interface Rol {
    id_rol: number;
    name: string;
}