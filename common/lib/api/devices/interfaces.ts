export interface Device {
    id_dispositivo: number;
    mac: string;
    last_known_hostname: string | null;
    last_known_ip_address: string;
    usuario_niu: number;
    owner_display_name: string;
}

export interface ListDevices {
    start: number;
    limit: number;
    count: number;
    pages: number;
    results: Device[];
}