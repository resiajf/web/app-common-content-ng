import { Omit } from '../../meta';

export interface ComputerIssueAvailabilityRange {
    from: string;
    to: string;
    maybe?: boolean;
}

export interface ComputerIssueAvailability {
    mon: ComputerIssueAvailabilityRange[];
    tue: ComputerIssueAvailabilityRange[];
    wed: ComputerIssueAvailabilityRange[];
    thu: ComputerIssueAvailabilityRange[];
    fri: ComputerIssueAvailabilityRange[];
    sat: ComputerIssueAvailabilityRange[];
    sun: ComputerIssueAvailabilityRange[];
}

export type ComputerIssueState = 'pending' | 'waiting' | 'working' | 'waiting-user' | 'solved' | 'discarded' | 'wont-fix';

export interface SmallComputerIssue {
    id_parte_informatica: number;
    is_wifi: boolean;
    is_wire: boolean;
    is_other: boolean;
    fecha_creacion: Date;
    state: ComputerIssueState;
    usuario_niu: number;
    issue_description: string;
}

export interface ListComputerIssues {
    start: number;
    limit: number;
    count: number;
    pages: number;
    results: SmallComputerIssue[];
}

export interface ComputerIssueTracking {
    id_seguimiento: number;
    date_time: string;
    message: string;
    from_state: ComputerIssueState;
    parte_informatica_id_parte_informatica: number;
}

export interface ComputerIssue extends SmallComputerIssue {
    availability_json: ComputerIssueAvailability;
    tracking: ComputerIssueTracking[];
}

export type NewComputerIssue = Omit<Omit<Omit<Omit<ComputerIssue, 'id_parte_informatica'>, 'fecha_creacion'>, 'state'>, 'tracking'>;
