// from https://stackoverflow.com/questions/41476063/typescript-remove-key-from-type-subtraction-type
// and https://github.com/Microsoft/TypeScript/issues/23724#issuecomment-384807714
/**
 * for literal unions
 * @example Sub<'Y' | 'X', 'X'> // === 'Y'
 */
export type Sub<
    O extends string,
    D extends string
> = {[K in O]: (Record<D, never> & Record<string, K>)[K]}[O];

//Same as above
/**
 * Remove the keys represented by the string union type D from the object type O.
 *
 * @example Omit<{a: number, b: string}, 'a'> // === {b: string}
 * @example Omit<{a: number, b: string}, keyof {a: number}> // === {b: string}
 */
export type Omit<
    O,
    D extends string
> = Pick<O, Sub<Extract<keyof O, string>, D>>;