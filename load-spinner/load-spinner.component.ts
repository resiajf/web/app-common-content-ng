import { Component } from '@angular/core';

@Component({
    templateUrl: './load-spinner.component.html',
    selector: 'app-load-spinner'
})
export class LoadSpinnerComponent {}
