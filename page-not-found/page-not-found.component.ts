import { Component, Inject } from '@angular/core';
import { I18NEXT_SERVICE, ITranslationService } from 'angular-i18next';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html'
})
export class PageNotFoundComponent {

    constructor(@Inject(I18NEXT_SERVICE) private i18n: ITranslationService) {}

    public randomSubtitle() {
        const a: string[] = this.i18n.t('not-found.subtitles', { returnObjects: true }) as any || [ '¿' ];
        const b = Math.trunc(a.length * Math.random());
        return a[b];
    }

    public goBack() {
        window.history.back();
    }

}
