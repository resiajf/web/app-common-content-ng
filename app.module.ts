/// <reference types="node" />
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app.component';
import { declarations, imports, module, providers } from '../angular.module.config';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UserService } from './user.service';
import { LoadSpinnerComponent } from './load-spinner/load-spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { I18NEXT_SERVICE, I18NextModule, ITranslationService } from 'angular-i18next';
const Backend = require('i18next-xhr-backend');
const LanguageDetector = require('i18next-browser-languagedetector');

// See https://github.com/Romanchuk/angular-i18next and https://www.i18next.com/translation-function/essentials
const i18next = (i18NextService: ITranslationService) => () => i18NextService
  .use(Backend)
  .use(LanguageDetector)
  .init({
    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json'
    },
    debug: process.env.NODE_ENV !== 'PRODUCTION',
    defaultNS: module,
    detection: {
      caches: ['localStorage'],
      lookupLocalStorage: 'app-panel:lang',
    },
    fallbackLng: 'es',
    fallbackNS: 'common',
    interpolation: {
      escapeValue: false,
    },
    ns: ['common', module],
    react: {
      wait: true
    }
  });

//This piece of code loads translations before Angular loading anything else
const i18nProvider = { provide: APP_INITIALIZER, useFactory: i18next, deps: [ I18NEXT_SERVICE ], multi: true };
const i18nLangProvider = { provide: LOCALE_ID, deps: [ I18NEXT_SERVICE ], useFactory: (i18n: ITranslationService) => i18n.language };


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        SidebarComponent,
        LoadSpinnerComponent,
        PageNotFoundComponent,
        ...declarations,
    ],
    imports: [
        BrowserModule,
        I18NextModule.forRoot(),
        AppRoutingModule,
        ...imports,
    ],
    providers: [ i18nProvider, i18nLangProvider, UserService, ...providers ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
