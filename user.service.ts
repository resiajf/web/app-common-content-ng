import { EventEmitter, Injectable } from '@angular/core';
import { User } from './redux/user/state';
import { module, anonymousModule } from '../angular.module.config';
import { Session } from './lib/session';

const getUser = async () => {
    try {
        return Session.convertToStatusUser(await Session.checkSession(), module);
    } catch(e) {
        if(e === null && anonymousModule) {
            return Session.anonymousUser(module);
        } else {
            window.location.assign('/');
        }
    }
};

@Injectable({
    providedIn: 'root',
})
export class UserService {

    private _user: User;
    private _evEmitter = new EventEmitter<User>();

    constructor() {
        getUser().then(user => {
            this._user = user;
            this._evEmitter.emit(user);
        });
    }

    public get user(): User {
        return this._user;
    }

    public get userReceived() { return this._evEmitter; }

    public closeSession() {
        this._user.niu = NaN;
        Session.deleteSession();
    }

}
